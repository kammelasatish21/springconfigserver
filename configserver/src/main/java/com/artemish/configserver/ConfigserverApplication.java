package com.artemish.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigserverApplication.class, args);
        System.out.println("Spring Cloud Config Server is running!");
        System.out.println("You can access the configuration properties at: http://localhost:8059/qftesting/dev");	}

}
